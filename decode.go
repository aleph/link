package link

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"math/big"

	"gitlab.com/aleph-project/form"
)

// NewDecoder creates and returns a StreamDecoder that reads from the specified reader
func NewDecoder(r io.Reader) StreamDecoder {
	return &streamDecoder{r: r}
}

// A StreamDecoder decodes from a stream
type StreamDecoder interface {
	Decode(ctx form.Context) (form.Form, error)
}

type streamDecoder struct {
	r     io.Reader
	data  []byte
	buf   [256]byte
	formd Decoder
}

func (d *streamDecoder) Decode(ctx form.Context) (form.Form, error) {
	for {
		if d.data == nil || len(d.data) == 0 {
			n, err := d.r.Read(d.buf[:])
			d.data = d.buf[:n]
			if err != nil {
				return nil, err
			}
		}

		if d.formd == nil {
			d.formd = new(FormDecoder)
		}

		n, value, next, err := d.formd.Decode(d.data, ctx)
		d.data = d.data[n:]
		d.formd = next
		if err != nil {
			return nil, err
		}
		if value != nil {
			return value.(form.Form), err
		}
	}
}

// A Decoder decodes values from a buffer
//
// Decode will read a full or partial value from the input data. It returns
// the number of bytes read, the decoded value, the next decoder, and any
// error encountered.
//
// If data is not empty and no error is returned, the number of bytes read
// must be greater than zero.
//
// If the input data is insufficient to decode a full value, the value
// returned will be nil and the next decoder can be used to complete the
// decoding.
//
// If the input data is sufficient to decode a full value, that value
// will be returned and the next decoder will be nil.
type Decoder interface {
	Decode(data []byte, ctx form.Context) (n int, value interface{}, next Decoder, err error)
}

func chainDecode(n int, data []byte, ctx form.Context, x Decoder) (int, interface{}, Decoder, error) {
	m, v, y, err := x.Decode(data[n:], ctx)
	return n + m, v, y, err
}

func primitiveChainDecode(n int, data []byte, ctx form.Context, x Decoder, factory func(interface{}) (form.Form, error)) (int, interface{}, Decoder, error) {
	return chainDecode(n, data, ctx, &primitiveFormDecoder{factory, x})
}

func generalChainDecode(n int, data []byte, ctx form.Context, factory func([]form.Form) (form.Form, error)) (int, interface{}, Decoder, error) {
	return chainDecode(n, data, ctx, &generalFormDecoder{factory, nil})
}

func tensorChainDecode(n int, data []byte, ctx form.Context, next func([]uint32) (Decoder, error)) (int, interface{}, Decoder, error) {
	return chainDecode(n, data, ctx, &tensorFormDecoder{next, nil})
}

func newNonconformingWellDefinedForm(head form.Head, forms []form.Form) form.Form {
	h := form.NewKnownSymbolicHead(uint64(head.Kind()), head.Name())
	f, _ := form.New(h, forms...)
	return f
}

// A FormDecoder decodes full or partial forms
type FormDecoder struct {
	headd Decoder
}

// Decode will decode full or partial forms
func (d *FormDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	headd := d.headd
	if headd == nil {
		headd = new(HeadDecoder)
	}

	n, value, next, err := headd.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &FormDecoder{next}
		}
		return n, value, next, err
	}

	head := value.(form.Head)
	switch head.Kind() {
	case form.NullHeadKind:
		return n, form.Null, nil, nil

	case form.SymbolHeadKind:
		return primitiveChainDecode(n, data, ctx, &StringDecoder{}, func(v interface{}) (form.Form, error) {
			return form.SymbolForm(v.(string)), nil
		})

	case form.StringHeadKind:
		return primitiveChainDecode(n, data, ctx, &StringDecoder{}, func(v interface{}) (form.Form, error) {
			return form.StringForm(v.(string)), nil
		})

	case form.BooleanHeadKind:
		return primitiveChainDecode(n, data, ctx, &BooleanDecoder{}, func(v interface{}) (form.Form, error) {
			return form.BooleanForm(v.(bool)), nil
		})

	case form.UUIDHeadKind:
		return primitiveChainDecode(n, data, ctx, &UUIDDecoder{}, func(v interface{}) (form.Form, error) {
			return form.UUIDForm(v.([16]byte)), nil
		})

	case form.IntegerHeadKind:
		return primitiveChainDecode(n, data, ctx, &VarintDecoder{}, func(v interface{}) (form.Form, error) {
			v, _ = convertFromZigZag(v)
			if u, ok := v.(*big.Int); ok {
				return (*form.LargeIntegerForm)(u), nil
			}
			return form.SmallIntegerForm(v.(int64)), nil
		})

	case form.RationalHeadKind:
		return primitiveChainDecode(n, data, ctx, &RationalDecoder{}, func(v interface{}) (form.Form, error) {
			if u, ok := v.(*big.Rat); ok {
				return (*form.LargeRationalForm)(u), nil
			}
			return form.SmallRationalForm(v.(rational)), nil
		})

	case form.RealHeadKind:
		return primitiveChainDecode(n, data, ctx, &RealDecoder{}, func(v interface{}) (form.Form, error) {
			if u, ok := v.(*big.Float); ok {
				return (*form.LargeRealForm)(u), nil
			}
			return form.SmallRealForm(v.(float64)), nil
		})

	case form.ContextHeadKind:
		return primitiveChainDecode(n, data, ctx, &VarintDecoder{}, func(v interface{}) (form.Form, error) {
			return form.ContextForm(uint32(v.(uint64))), nil
		})

	case form.TensorHeadKind:
		return tensorChainDecode(n, data, ctx, func(dims []uint32) (Decoder, error) {
			return &generalFormDecoder2{func(forms []form.Form) (form.Form, error) {
				return form.NewTensor(dims, forms...), nil
			}, make([]form.Form, 0, getDepthFromDimensions(dims)), nil}, nil
		})

	case form.IntegerTensorHeadKind:
		return tensorChainDecode(n, data, ctx, func(dims []uint32) (Decoder, error) {
			return &varintTensorDecoder{dims, make([]int64, 0, getDepthFromDimensions(dims)), nil}, nil
		})

	case form.RationalTensorHeadKind:
		return tensorChainDecode(n, data, ctx, func(dims []uint32) (Decoder, error) {
			return &rationalTensorDecoder{dims, make([]rational, 0, getDepthFromDimensions(dims)), nil}, nil
		})

	case form.RealTensorHeadKind:
		return tensorChainDecode(n, data, ctx, func(dims []uint32) (Decoder, error) {
			return &realTensorDecoder{dims}, nil
		})

	case form.HeadHeadKind:
		return generalChainDecode(n, data, ctx, func(forms []form.Form) (form.Form, error) {
			if len(forms) == 1 {
				return &form.HeadForm{Form: forms[0]}, nil
			}
			return newNonconformingWellDefinedForm(head, forms), nil
		})

	case form.SymbolicHeadKind, form.PartHeadKind, form.PacketHeadKind, form.FormHeadKind:
		return generalChainDecode(n, data, ctx, func(forms []form.Form) (form.Form, error) {
			return form.New(head, forms...)
		})

	default:
		return n, nil, nil, fmt.Errorf("unsupported head kind: %v", head.Kind())
	}
}

type primitiveFormDecoder struct {
	factory func(interface{}) (form.Form, error)
	valued  Decoder
}

func (d *primitiveFormDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	valued := d.valued

	n, value, next, err := valued.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &primitiveFormDecoder{d.factory, next}
		}
		return n, value, next, err
	}

	f, err := d.factory(value)
	return n, f, nil, err
}

type tensorFormDecoder struct {
	next  func([]uint32) (Decoder, error)
	dimsd Decoder
}

func (d *tensorFormDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	dimsd := d.dimsd
	if dimsd == nil {
		dimsd = &VarintDecoder{uint64(MaxInt), 0, 0}
	}

	n, value, next, err := dimsd.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &tensorFormDecoder{d.next, next}
		}
		return n, value, next, err
	}

	dims := value.(uint64)
	return chainDecode(n, data, ctx, &tensorFormDecoder2{d.next, make([]uint32, 0, dims), nil})
}

type tensorFormDecoder2 struct {
	next func([]uint32) (Decoder, error)
	dims []uint32
	dimd Decoder
}

func (d *tensorFormDecoder2) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	dims := d.dims
	dimd := d.dimd
	if dimd == nil {
		dimd = &VarintDecoder{math.MaxUint32, 0, 0}
	}

	var N = 0
	for len(dims) < cap(dims) {
		n, value, next, err := dimd.Decode(data, ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &tensorFormDecoder2{d.next, dims, dimd}
			}
			return N, value, next, err
		}

		dims = append(dims, uint32(value.(uint64)))
		dimd = &VarintDecoder{math.MaxUint32, 0, 0}
	}

	next, err := d.next(dims)
	if err != nil {
		return N, nil, nil, err
	}
	return chainDecode(N, data, ctx, next)
}

type generalFormDecoder struct {
	factory func([]form.Form) (form.Form, error)
	lend    Decoder
}

func (d *generalFormDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	lend := d.lend
	if lend == nil {
		lend = &VarintDecoder{uint64(MaxInt), 0, 0}
	}

	n, value, next, err := lend.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &generalFormDecoder{d.factory, next}
		}
		return n, value, next, err
	}

	len := int(value.(uint64))
	return chainDecode(n, data, ctx, &generalFormDecoder2{d.factory, make([]form.Form, 0, len), nil})
}

type generalFormDecoder2 struct {
	factory func([]form.Form) (form.Form, error)
	forms   []form.Form
	formd   Decoder
}

func (d *generalFormDecoder2) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	forms := d.forms
	formd := d.formd
	if formd == nil {
		formd = new(FormDecoder)
	}

	var N = 0
	for len(forms) < cap(forms) {
		n, value, next, err := formd.Decode(data[N:], ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &generalFormDecoder2{d.factory, forms, next}
			}
			return N, value, next, err
		}

		forms = append(forms, value.(form.Form))
		formd = new(FormDecoder)
	}

	f, err := d.factory(forms)
	return N, f, nil, err
}

// A HeadDecoder decodes full or partial form heads
type HeadDecoder struct {
	idd Decoder
}

// Decode will decode full or partial form heads; see Decoder
func (d *HeadDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	idd := d.idd
	if idd == nil {
		idd = &VarintDecoder{math.MaxUint64, 0, 0}
	}

	n, value, next, err := idd.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &HeadDecoder{next}
		}
		return n, value, next, err
	}

	id := value.(uint64)
	if id > math.MaxUint64 {
		return n, nil, nil, fmt.Errorf("head ID larger than allowed")
	}

	if id == 0 {
		return chainDecode(n, data, ctx, new(unknownHeadDecoder))
	}

	if id == uint64(form.FormHeadKind) {
		return chainDecode(n, data, ctx, new(formHeadDecoder))
	}

	if id <= uint64(form.MaxHeadKind) {
		kind := form.HeadKind(id)
		head, ok := form.GetHeadForKind(kind)
		if !ok {
			return n, nil, nil, fmt.Errorf("Unsupported head kind: %v", kind)
		}
		return n, head, nil, nil
	}

	name, ok := ctx.GetKnownHeadName(id)
	if !ok {
		return n, nil, nil, fmt.Errorf("head ID is not known: %d", id)
	}

	return n, form.NewKnownSymbolicHead(id, name), nil, nil
}

type unknownHeadDecoder struct {
	named Decoder
}

func (d *unknownHeadDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	named := d.named
	if named == nil {
		named = &StringDecoder{}
	}

	n, value, next, err := named.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &unknownHeadDecoder{next}
		}
		return n, value, next, err
	}

	name := value.(string)
	return n, form.NewUnknownSymbolicHead(name), nil, nil
}

type formHeadDecoder struct {
	formd Decoder
}

func (d *formHeadDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	formd := d.formd
	if formd == nil {
		formd = new(FormDecoder)
	}

	n, value, next, err := formd.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &formHeadDecoder{next}
		}
		return n, value, next, err
	}

	f := value.(form.Form)
	return n, form.NewFormHead(f), nil, nil
}

// A VarintDecoder decodes full or partial varints
type VarintDecoder struct {
	Limit uint64
	size  uint8
	value uint64
}

// Decode will decode full or partial varints; see Decoder
func (d *VarintDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	if len(data) == 0 {
		return 0, nil, d, nil
	}

	size := d.size
	value := d.value

	var n = 0
	var more = true
	for n < len(data) && more {
		if size > 56 {
			if d.Limit != 0 {
				return n, nil, nil, errors.New("Varint exceeded decode size")
			}
			next := &largeVarintDecoder{uint(size), big.NewInt(int64(value))}
			return chainDecode(n, data, ctx, next)
		}

		b := data[n]
		more = b&0x80 != 0
		value += uint64(b&0x7F) << size
		size += 7
		if d.Limit != 0 && value > d.Limit {
			return n, nil, nil, errors.New("Varint exceeded decode size")
		}

		n++
	}

	if !more {
		return n, value, nil, nil
	}

	next := &VarintDecoder{d.Limit, size, value}
	return n, nil, next, nil
}

type largeVarintDecoder struct {
	size  uint
	value *big.Int
}

func (d *largeVarintDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	if len(data) == 0 {
		return 0, nil, d, nil
	}

	size := d.size
	value := d.value

	var n = 0
	var more = true
	for n < len(data) && more {
		b := data[n]
		more = b&0x80 != 0
		v := big.NewInt(int64(b & 0x7F))
		n++

		if size > MaxUint-7 {
			return n, nil, nil, errors.New("Exceeded varint decoding capacity")
		}

		u := new(big.Int).Lsh(v, size)
		value = new(big.Int).Add(value, u)
	}

	if !more {
		return n, value, nil, nil
	}

	next := &largeVarintDecoder{size, value}
	return n, nil, next, nil
}

// A StringDecoder decodes full or partial strings
type StringDecoder struct {
	sized Decoder
}

// Decode will decode full or partial strings; see Decoder
func (d *StringDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	sized := d.sized
	if sized == nil {
		sized = &VarintDecoder{uint64(MaxInt), 0, 0}
	}

	n, value, next, err := sized.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &StringDecoder{next}
		}
		return n, value, next, err
	}

	size := value.(uint64)
	if size > uint64(MaxInt) {
		return n, nil, nil, errors.New("String length is too long")
	}

	next = &stringDataDecoder{int(size), nil}
	return chainDecode(n, data, ctx, next)
}

type stringDataDecoder struct {
	size  int
	chars []byte
}

func (d *stringDataDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	size := d.size
	chars := d.chars
	if chars == nil {
		chars = []byte{}
	}
	want := size - len(chars)

	if want <= len(data) {
		chars = append(chars, data[:want]...)
		return want, string(chars), nil, nil
	}

	chars = append(chars, data...)
	return len(data), nil, &stringDataDecoder{size, chars}, nil
}

// A BooleanDecoder decodes booleans
type BooleanDecoder struct{}

// Decode will decode booleans; see Decoder
func (d *BooleanDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	if len(data) == 0 {
		return 0, nil, d, nil
	}

	b := data[0]
	if b == 0 {
		return 1, false, nil, nil
	}
	if b == 1 {
		return 1, true, nil, nil
	}
	return 1, nil, nil, fmt.Errorf("invalid boolean value: %d", b)
}

type UUIDDecoder struct {
	index int
	bytes [16]byte
}

func (d *UUIDDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	index := d.index
	bytes := d.bytes
	dst := bytes[index:]
	want := 16 - index

	if want <= len(data) {
		copy(dst, data[:want])
		return want, bytes, nil, nil
	}

	copy(dst, data)
	index += len(data)
	return len(data), nil, &UUIDDecoder{index, bytes}, nil
}

// A RationalDecoder decodes full or partial rationals
type RationalDecoder struct {
	numd Decoder
}

// Decode will decode full or partial rationals
func (d *RationalDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	numd := d.numd
	if numd == nil {
		numd = new(VarintDecoder)
	}

	n, value, next, err := numd.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &RationalDecoder{next}
		}
		return n, value, next, err
	}

	numerator, _ := convertFromZigZag(value)
	next = &rationalDecoder2{numerator, nil}
	return chainDecode(n, data, ctx, next)
}

type rationalDecoder2 struct {
	numerator interface{}
	denomd    Decoder
}

func (d *rationalDecoder2) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	denomd := d.denomd
	if denomd == nil {
		denomd = new(VarintDecoder)
	}

	n, value, next, err := denomd.Decode(data, ctx)
	if err != nil || n == 0 || value == nil {
		if next != nil {
			next = &RationalDecoder{next}
		}
		return n, value, next, err
	}

	numerator := d.numerator
	denominator, _ := convertFromZigZag(value)
	largenum, nok := numerator.(*big.Int)
	largedenom, dok := denominator.(*big.Int)

	if nok && !dok {
		largedenom = big.NewInt(denominator.(int64))
		dok = true
	} else if !nok && dok {
		largenum = big.NewInt(numerator.(int64))
		nok = true
	}

	if nok && dok {
		return n, new(big.Rat).SetFrac(largenum, largedenom), nil, nil
	}

	smallnum := numerator.(int64)
	smalldenom := denominator.(int64)

	if smalldenom < 0 {
		smallnum *= -1
		smalldenom *= -1
	}

	return n, rational{
		Numerator:   smallnum,
		Denominator: uint64(smalldenom),
	}, nil, nil
}

// A RealDecoder decodes full or partial reals
type RealDecoder struct{}

// Decode will decode full or partial reals; see Decoder
func (d *RealDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	if len(data) == 0 {
		return 0, nil, d, nil
	}

	format, value := DecodeRealHeader(data[0])
	if value == EncodedRealValue {
		switch format {
		case ArbitraryRealFormat:
			return chainDecode(1, data, ctx, &realDecoder2{format, make([]byte, 0, 0)})
		case RealBits16Base2Format:
			return chainDecode(1, data, ctx, &realDecoder2{format, make([]byte, 0, 2)})
		case RealBits32Base2Format:
			return chainDecode(1, data, ctx, &realDecoder2{format, make([]byte, 0, 4)})
		case RealBits64Base2Format:
			return chainDecode(1, data, ctx, &realDecoder2{format, make([]byte, 0, 8)})
		case RealBits128Base2Format:
			return chainDecode(1, data, ctx, &realDecoder2{format, make([]byte, 0, 16)})
		case RealBits256Base2Format:
			return chainDecode(1, data, ctx, &realDecoder2{format, make([]byte, 0, 32)})
		default:
			return 1, nil, nil, fmt.Errorf("invalid real number format: %v", format)
		}
	}

	if format != RealBits64Base2Format {
		return 1, nil, nil, errors.New("only 64-bit base-2 IEEE754-encoded reals are currently supported")
	}

	switch value {
	case RealPosZeroValue:
		return 1, form.SmallRealForm(realPosZeroValue), nil, nil
	case RealNegZeroValue:
		return 1, form.SmallRealForm(realNegZeroValue), nil, nil
	case RealPosInfValue:
		return 1, form.SmallRealForm(realPosInfValue), nil, nil
	case RealNegInfValue:
		return 1, form.SmallRealForm(realNegInfValue), nil, nil
	case RealPosNaNValue:
		return 1, form.SmallRealForm(realPosNaNValue), nil, nil
	case RealNegNaNValue:
		return 1, form.SmallRealForm(realNegNaNValue), nil, nil
	default:
		return 1, nil, nil, fmt.Errorf("invalid real number value specifier: %v", value)
	}
}

type realDecoder2 struct {
	format RealFormat
	bytes  []byte
}

// Decode will decode full or partial reals; see Decoder
func (d *realDecoder2) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	if d.format != RealBits64Base2Format {
		return 1, nil, nil, errors.New("only 64-bit base-2 IEEE754-encoded reals are currently supported")
	}

	if len(data) == 0 {
		return 0, nil, d, nil
	}

	bytes := d.bytes
	want := cap(bytes) - len(bytes)
	if len(data) < want {
		return want, nil, &realDecoder2{d.format, append(bytes, data...)}, nil
	}

	bytes = append(bytes, data[:want]...)
	value := math.Float64frombits(binary.BigEndian.Uint64(bytes))
	return want, value, nil, nil
}

type varintTensorDecoder struct {
	dims   []uint32
	values []int64
	valued Decoder
}

func (d *varintTensorDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	values := d.values
	valued := d.valued
	if valued == nil {
		valued = new(VarintDecoder)
	}

	var N = 0
	for len(values) < cap(values) {
		n, value, next, err := valued.Decode(data, ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &varintTensorDecoder{d.dims, values, valued}
			}
			return N, value, next, err
		}

		value, _ = convertFromZigZag(value)

		if _, ok := value.(int64); !ok {
			forms := append(informInts(values), (*form.LargeIntegerForm)(value.(*big.Int)))
			return chainDecode(N, data, ctx, &varintTensorDecoder2{d.dims, forms, nil})
		}

		values = append(values, value.(int64))
		valued = new(VarintDecoder)
	}

	return N, form.NewIntegerTensor(d.dims, values), nil, nil
}

func informInts(v []int64) []form.Form {
	x := make([]form.Form, len(v), cap(v))
	for i, u := range v {
		x[i] = form.SmallIntegerForm(u)
	}
	return x
}

type varintTensorDecoder2 struct {
	dims   []uint32
	values []form.Form
	valued Decoder
}

func (d *varintTensorDecoder2) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	values := d.values
	valued := d.valued
	if valued == nil {
		valued = new(VarintDecoder)
	}

	var N = 0
	for len(values) < cap(values) {
		n, value, next, err := valued.Decode(data, ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &varintTensorDecoder2{d.dims, values, valued}
			}
			return N, value, next, err
		}

		value, _ = convertFromZigZag(value)
		if big, ok := value.(*big.Int); ok {
			values = append(values, (*form.LargeIntegerForm)(big))
		} else {
			values = append(values, form.SmallIntegerForm(value.(int64)))
		}

		valued = new(VarintDecoder)
	}

	return N, form.NewTensor(d.dims, values...), nil, nil
}

type rationalTensorDecoder struct {
	dims   []uint32
	values []rational
	valued Decoder
}

func (d *rationalTensorDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	values := d.values
	valued := d.valued
	if valued == nil {
		valued = new(RationalDecoder)
	}

	var N = 0
	for len(values) < cap(values) {
		n, value, next, err := valued.Decode(data, ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &rationalTensorDecoder{d.dims, values, valued}
			}
			return N, value, next, err
		}

		value, _ = convertFromZigZag(value)

		if _, ok := value.(rational); !ok {
			forms := append(informRats(values), (*form.LargeRationalForm)(value.(*big.Rat)))
			return chainDecode(N, data, ctx, &rationalTensorDecoder2{d.dims, forms, nil})
		}

		values = append(values, value.(rational))
		valued = new(RationalDecoder)
	}

	return N, form.NewRationalTensor(d.dims, values), nil, nil
}

func informRats(v []rational) []form.Form {
	x := make([]form.Form, len(v), cap(v))
	for i, u := range v {
		x[i] = form.SmallRationalForm(u)
	}
	return x
}

type rationalTensorDecoder2 struct {
	dims   []uint32
	values []form.Form
	valued Decoder
}

func (d *rationalTensorDecoder2) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	values := d.values
	valued := d.valued
	if valued == nil {
		valued = new(RationalDecoder)
	}

	var N = 0
	for len(values) < cap(values) {
		n, value, next, err := valued.Decode(data, ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &rationalTensorDecoder2{d.dims, values, valued}
			}
			return N, value, next, err
		}

		value, _ = convertFromZigZag(value)
		if big, ok := value.(*big.Rat); ok {
			values = append(values, (*form.LargeRationalForm)(big))
		} else {
			values = append(values, form.SmallRationalForm(value.(rational)))
		}

		valued = new(RationalDecoder)
	}

	return N, form.NewTensor(d.dims, values...), nil, nil
}

type realTensorDecoder struct {
	dims []uint32
}

func (d *realTensorDecoder) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	if len(data) == 0 {
		return 0, nil, d, nil
	}

	format, value := DecodeRealHeader(data[0])
	if format != RealBits64Base2Format {
		return 1, nil, nil, errors.New("only 64-bit base-2 IEEE754-encoded reals are currently supported")
	}

	if value == EncodedRealValue {
		return chainDecode(1, data, ctx, &realTensorDecoder64{d.dims, make([]float64, 0, getDepthFromDimensions(d.dims)), nil})
	}

	switch value {
	case RealPosZeroValue, RealNegZeroValue, RealPosInfValue, RealNegInfValue, RealPosNaNValue, RealNegNaNValue:
		return 1, makeTrivialFloatTensor(d.dims, ExpandRealValue64(value)), nil, nil
	default:
		return 1, nil, nil, fmt.Errorf("invalid real number value specifier: %v", value)
	}
}

func makeTrivialFloatTensor(dims []uint32, value float64) form.Form {
	values := make([]float64, getDepthFromDimensions(dims))
	for i := range values {
		values[i] = value
	}
	return form.NewRealTensor(dims, values)
}

type realTensorDecoder64 struct {
	dims   []uint32
	values []float64
	valued Decoder
}

func (d *realTensorDecoder64) Decode(data []byte, ctx form.Context) (int, interface{}, Decoder, error) {
	values := d.values
	valued := d.valued
	if valued == nil {
		valued = &realDecoder2{RealBits64Base2Format, make([]byte, 0, 8)}
	}

	var N = 0
	for len(values) < cap(values) {
		n, value, next, err := valued.Decode(data, ctx)
		N += n
		if err != nil || n == 0 || value == nil {
			if next != nil {
				next = &realTensorDecoder64{d.dims, values, valued}
			}
			return N, value, next, err
		}

		values = append(values, value.(float64))
		valued = &realDecoder2{RealBits64Base2Format, make([]byte, 0, 8)}
	}

	return N, form.NewRealTensor(d.dims, values), nil, nil
}
