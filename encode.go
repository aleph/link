package link

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"math/big"

	"gitlab.com/aleph-project/form"
)

// NewEncoder creates and returns a StreamEncoder that writes to the specified writer
func NewEncoder(w io.Writer) StreamEncoder {
	return &streamEncoder{w}
}

// A StreamEncoder encodes to a stream
type StreamEncoder interface {
	Encode(form form.Form, ctx form.Context) error
}

type streamEncoder struct {
	w io.Writer
}

func (d *streamEncoder) Encode(f form.Form, ctx form.Context) error {
	e, err := NewFormEncoder(f)
	if err != nil {
		return err
	}

	var buf = [256]byte{}
	for e != nil {
		n, next, err := e.Encode(buf[:], ctx)
		if err != nil {
			return err
		}
		if n == 0 {
			return errors.New("encoded nothing")
		}
		e = next

		data := buf[:n]
		for len(data) > 0 {
			m, err := d.w.Write(data)
			if err != nil {
				return err
			}
			if m == 0 {
				return errors.New("wrote nothing")
			}
			data = data[m:]
		}
	}

	return nil
}

// A Encoder encodes a value to a buffer
//
// Encode will encode a full or partial value to the output buffer. It returns
// the number of bytes read, the next encoder, and any error encountered.
//
// If the buffer has space and no error is returned, the number of bytes read
// must be greater than zero.
//
// If the available space in the buffer is insufficient to encode a full value,
// the next encoder can be used to complete the encoding. Otherwise, the next
// encoder will be nil.
type Encoder interface {
	Encode(buf []byte, ctx form.Context) (n int, next Encoder, err error)
}

// NewFormEncoder returns an encoder that will encode the form
func NewFormEncoder(value form.Form) (Encoder, error) {
	head := value.Head()
	he, err := NewHeadEncoder(head)
	if err != nil {
		return nil, err
	}

	chain := func(e Encoder) Encoder {
		return &chainEncoder{he, e}
	}

	switch head.Kind() {
	case form.NullHeadKind:
		if _, ok := value.(form.NullForm); ok {
			return he, nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.SymbolHeadKind:
		if f, ok := value.(form.SymbolForm); ok {
			return chain(NewStringEncoder(string(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.StringHeadKind:
		if f, ok := value.(form.StringForm); ok {
			return chain(NewStringEncoder(string(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.BooleanHeadKind:
		if f, ok := value.(form.BooleanForm); ok {
			return chain(NewBooleanEncoder(bool(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.UUIDHeadKind:
		if f, ok := value.(form.UUIDForm); ok {
			v := [16]byte(f)
			return chain(&bytesEncoder{v[:]}), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.IntegerHeadKind:
		if f, ok := value.(form.SmallIntegerForm); ok {
			v, _ := convertToZigZag(int64(f))
			return chain(NewVarintEncoder(v.(uint64))), nil
		}
		if f, ok := value.(*form.LargeIntegerForm); ok {
			v, _ := convertToZigZag((*big.Int)(f))
			return chain(NewLargeVarintEncoder(v.(*big.Int))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.RationalHeadKind:
		if f, ok := value.(form.SmallRationalForm); ok {
			return chain(NewRationalEncoder(rational(f))), nil
		}
		if f, ok := value.(*form.LargeRationalForm); ok {
			return chain(NewLargeRationalEncoder((*big.Rat)(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.RealHeadKind:
		if f, ok := value.(form.SmallRealForm); ok {
			return chain(NewReal64Encoder(float64(f))), nil
		}
		if f, ok := value.(*form.LargeRealForm); ok {
			return chain(NewLargeRealEncoder((*big.Float)(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.ContextHeadKind:
		if f, ok := value.(form.ContextForm); ok {
			return chain(NewVarintEncoder(uint64(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.TensorHeadKind:
		if f, ok := value.(form.TensorForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), nil, makeElementsGetter(f))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.IntegerTensorHeadKind:
		if f, ok := value.(form.IntegerListForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), nil, makeIntegersGetter(f))), nil
		}
		if f, ok := value.(form.IntegerTensorForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), nil, makeIntegersGetter(f.IntegerListForm))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.RationalTensorHeadKind:
		if f, ok := value.(form.RationalListForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), nil, makeRationalsGetter(f))), nil
		}
		if f, ok := value.(form.RationalTensorForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), nil, makeRationalsGetter(f.RationalListForm))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.RealTensorHeadKind:
		if f, ok := value.(form.RealListForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), makeRealHeaderEncoder(RealBits64Base2Format, EncodedRealValue), makeRealsGetter(f))), nil
		}
		if f, ok := value.(form.RealTensorForm); ok {
			return chain(makeTensorEncoder(f.GetDimensions(), makeRealHeaderEncoder(RealBits64Base2Format, EncodedRealValue), makeRealsGetter(f.RealListForm))), nil
		}
		return nil, fmt.Errorf("Unexpected value for kind %v: %v", head.Kind(), value)

	case form.HeadHeadKind, form.SymbolicHeadKind, form.PartHeadKind, form.PacketHeadKind, form.FormHeadKind:
		return chain(makeElementsEncoder(value)), nil

	default:
		return nil, fmt.Errorf("unsupported head kind: %v", head.Kind())
	}
}

func chainEncode(n int, buf []byte, ctx form.Context, e Encoder) (int, Encoder, error) {
	m, next, err := e.Encode(buf[n:], ctx)
	return n + m, next, err
}

type chainEncoder struct {
	current, next Encoder
}

func (e *chainEncoder) Encode(buf []byte, ctx form.Context) (int, Encoder, error) {
	var n = 0

start:
	if len(buf[n:]) == 0 {
		return n, e, nil
	}

	m, next, err := e.current.Encode(buf[n:], ctx)
	n += m
	if err != nil {
		return n, nil, err
	}
	if next != nil {
		return n, &chainEncoder{next, e.next}, nil
	}

	// efficiently handle long chains
	if ch, ok := e.next.(*chainEncoder); ok {
		e = ch
		goto start
	}

	return chainEncode(n, buf, ctx, e.next)
}

func makeElementsEncoder(f form.Form) Encoder {
	return &chainEncoder{
		NewVarintEncoder(uint64(f.Depth())),
		&repeatEncoder{
			length: int(f.Depth()),
			get:    makeElementsGetter(f),
		},
	}
}

func makeElementsGetter(f form.Form) func(int) (Encoder, error) {
	return func(i int) (Encoder, error) {
		e, ok := f.GetElement(uint32(i))
		if !ok {
			return nil, errors.New("Number of form elements is less than form depth")
		}
		return NewFormEncoder(e)
	}
}

func makeIntegersGetter(f form.IntegerListForm) func(int) (Encoder, error) {
	return func(i int) (Encoder, error) {
		if i >= len(f) {
			return nil, errors.New("Number of form elements is less than form depth")
		}
		v, _ := convertToZigZag(f[i])
		return NewVarintEncoder(v.(uint64)), nil
	}
}

func makeRationalsGetter(f form.RationalListForm) func(int) (Encoder, error) {
	return func(i int) (Encoder, error) {
		if i >= len(f) {
			return nil, errors.New("Number of form elements is less than form depth")
		}
		return NewRationalEncoder(f[i]), nil
	}
}

func makeRealsGetter(f form.RealListForm) func(int) (Encoder, error) {
	return func(i int) (Encoder, error) {
		if i >= len(f) {
			return nil, errors.New("Number of form elements is less than form depth")
		}
		return makeRealEncoder64B2(f[i]), nil
	}
}

func makeTensorEncoder(dims []uint32, itemsHeader Encoder, get func(int) (Encoder, error)) Encoder {
	dimensionality := NewVarintEncoder(uint64(len(dims)))
	theader := &repeatEncoder{
		length: len(dims),
		get: func(i int) (Encoder, error) {
			return NewVarintEncoder(uint64(dims[i])), nil
		},
	}
	items := &repeatEncoder{
		length: int(getDepthFromDimensions(dims)),
		get:    get,
	}
	if itemsHeader == nil {
		return &chainEncoder{dimensionality, &chainEncoder{theader, items}}
	}
	return &chainEncoder{dimensionality, &chainEncoder{theader, &chainEncoder{itemsHeader, items}}}
}

type repeatEncoder struct {
	index   int
	length  int
	get     func(int) (Encoder, error)
	current Encoder
}

func (e *repeatEncoder) Encode(buf []byte, ctx form.Context) (int, Encoder, error) {
	if len(buf) == 0 {
		return 0, e, nil
	}

	var err error
	var n, N int
	var i = e.index
	var current = e.current
	for i < e.length {
		if current == nil {
			current, err = e.get(i)
			if err != nil {
				return N, nil, err
			}
		}

		n, current, err = current.Encode(buf, ctx)
		N += n
		buf = buf[n:]
		if err != nil {
			return N, nil, err
		}
		if current != nil {
			return N, &repeatEncoder{i, e.length, e.get, current}, nil
		}
		i++
	}

	return N, nil, nil
}

// NewHeadEncoder returns an encoder that will encode the head
func NewHeadEncoder(value form.Head) (Encoder, error) {
	if fh, ok := value.(form.FormHead); ok {
		e, err := NewFormEncoder(fh.Form())
		if err != nil {
			return nil, err
		}
		return &chainEncoder{&varintEncoder{uint64(value.Kind())}, e}, nil
	}

	if value.Kind() != form.SymbolicHeadKind {
		return &varintEncoder{uint64(value.Kind())}, nil
	}

	id := value.(form.SymbolicHead).ID()
	if id != 0 {
		return &varintEncoder{id}, nil
	}

	return &chainEncoder{
		&varintEncoder{0},
		NewStringEncoder(value.Name()),
	}, nil
}

// NewVarintEncoder returns an encoder that will encode the integer value using
// varint encoding
func NewVarintEncoder(value uint64) Encoder {
	return &varintEncoder{value}
}

type varintEncoder struct {
	value uint64
}

func (e *varintEncoder) Encode(buf []byte, ctx form.Context) (int, Encoder, error) {
	if len(buf) == 0 {
		return 0, e, nil
	}

	var value = e.value
	var n = 0
	for n < len(buf) {
		buf[n] = byte(value & 0x7F)
		value >>= 7

		if value > 0 {
			buf[n] |= 0x80
			n++
		} else {
			return n + 1, nil, nil
		}
	}

	return n, &varintEncoder{value}, nil
}

// NewStringEncoder returns an encoder that will encode the string
func NewStringEncoder(value string) Encoder {
	return &chainEncoder{
		&varintEncoder{uint64(len(value))},
		&bytesEncoder{[]byte(value)},
	}
}

// NewBooleanEncoder returns an encoder that will encode the boolean
func NewBooleanEncoder(value bool) Encoder {
	if value {
		return &bytesEncoder{[]byte{1}}
	}
	return &bytesEncoder{[]byte{0}}
}

type bytesEncoder struct {
	value []byte
}

func (e *bytesEncoder) Encode(buf []byte, ctx form.Context) (int, Encoder, error) {
	if len(buf) == 0 {
		return 0, e, nil
	}

	n := copy(buf, e.value)
	if n == len(e.value) {
		return n, nil, nil
	}

	return n, &bytesEncoder{e.value[n:]}, nil
}

// NewLargeVarintEncoder returns an encoder that will encode the integer value using
// varint encoding
func NewLargeVarintEncoder(value *big.Int) Encoder {
	return &largeVarintEncoder{value}
}

type largeVarintEncoder struct {
	value *big.Int
}

func (e *largeVarintEncoder) Encode(buf []byte, ctx form.Context) (int, Encoder, error) {
	if len(buf) == 0 {
		return 0, e, nil
	}

	var value = new(big.Int).Add(e.value, big.NewInt(0)) // create a copy
	var n = 0
	for n < len(buf) {
		buf[n] = byte(value.Bits()[0] & 0x7F)
		value.Rsh(value, 7)
		if value.IsUint64() && value.Uint64() == 0 {
			return n, nil, nil
		}

		buf[n] |= 0x80
	}

	return n, &largeVarintEncoder{value}, nil
}

// NewRationalEncoder returns an encoder that will encode the rational value using
// varint encoding for the numerator and denominator
func NewRationalEncoder(value rational) Encoder {
	num, _ := convertToZigZag(value.Numerator)
	denom, _ := convertToZigZag(value.Denominator)
	return &chainEncoder{
		NewVarintEncoder(num.(uint64)),
		NewVarintEncoder(denom.(uint64)),
	}
}

// NewLargeRationalEncoder returns an encoder that will encode the rational value using
// varint encoding for the numerator and denominator
func NewLargeRationalEncoder(value *big.Rat) Encoder {
	num, _ := convertToZigZag(value.Num())
	denom, _ := convertToZigZag(value.Denom())
	return &chainEncoder{
		NewLargeVarintEncoder(num.(*big.Int)),
		NewLargeVarintEncoder(denom.(*big.Int)),
	}
}

// NewReal64Encoder returns an encoder that will encode the real value using IEEE754
func NewReal64Encoder(value float64) Encoder {
	v := CompressRealValue64(value)
	header := makeRealHeaderEncoder(RealBits64Base2Format, v)
	if v != EncodedRealValue {
		return header
	}
	return &chainEncoder{header, makeRealEncoder64B2(value)}
}

// NewLargeRealEncoder will panic because it has not been implemented
func NewLargeRealEncoder(value *big.Float) Encoder {
	panic("not implemented")
}

func makeRealHeaderEncoder(f RealFormat, v RealValue) Encoder {
	return &bytesEncoder{[]byte{EncodeRealHeader(f, v)}}
}

func makeRealEncoder64B2(value float64) Encoder {
	bytes := [8]byte{}
	binary.BigEndian.PutUint64(bytes[:], math.Float64bits(value))
	return &bytesEncoder{bytes[:]}
}
