package link

import "gitlab.com/aleph-project/form/value"

type rational = value.Rational

// MaxUint is the maximum value of a uint
const MaxUint = ^uint(0)

// MaxInt is the maximum value of an int
const MaxInt = int(^uint(0) >> 1)

func getDepthFromDimensions(dims []uint32) uint32 {
	if len(dims) == 0 {
		return 0
	}

	var depth uint32 = 1
	for _, dim := range dims {
		depth *= dim
	}

	return depth
}
