# Aleph: Link

[![GoDoc](https://godoc.org/gitlab.com/aleph-project/link?status.svg)](https://godoc.org/gitlab.com/aleph-project/link)

The link between systems; aka the protocol.

## Form Packet

A form begins with the head kind `8-bit`.

If the head kind is `known`, then it is followed by the head ID `varint`, max uint32.

If the head kind is `unknown`, then it is followed by the head name `string`.

Encoding of the elements differs per head kind. Data for...

  * Simple primitives is the primitive value
  * Primitive tensors is dimensionality `varint` followed by that many dimensions `varint` followed by that many primitives
  * Some kinds is nothing
  * Some kinds is a fixed number of primitives as data
  * Some kinds is a fixed number of forms as data
  * All other kinds is element count `varint` followed by that many forms

## Primitives

Strings are length (in bytes) `varint` followed by UTF-8 characters.

Integer numbers are zig-zag signed varint.

Rational numbers are zig-zag signed varint, followed by unsigned varint.

Real numbers are real header and the encoded value. The header indicates the format, and can allow special values. Special values have no encoded value. For encoded values, the encoding is indicated by the format. Currently IEEE base-2 32-bit and 64-bit floats are supported.

## Tensors

Real tensors only have a single real header, after the tensor header, instead of one real header per value.

## Notes

  * Capability for custom form types to encode themselves?
    - Encoding: custom kind, head (string), content
    - `Un`/`MarshalBinary`, write # of bytes followed by bytes
    - `Un`/`MarshalForm`, un/marshal from/to forms, which are then coded the normal way
    - Byte array forms plus `Un`/`MarshalForm` would mostly obviate the need for `Un`/`MarshalBinary`