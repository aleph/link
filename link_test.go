package link

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"testing"

	"github.com/alecthomas/assert"
	"gitlab.com/aleph-project/form"
)

func must(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err)
	}
}

type _context struct{}

func (c *_context) GetKnownHeadName(id uint64) (name string, ok bool) {
	return "", false
}

func (c *_context) GetKnownHeadID(name string) (id uint64, ok bool) {
	return 0, false
}

func TestBasic(t *testing.T) {
	ctx := new(_context)

	var tests = map[string]form.Form{
		"Test":   form.Must(form.New(form.NewUnknownSymbolicHead("Test"), form.SmallIntegerForm(1))),
		"Packet": form.Must(form.New(form.PacketHead, form.SmallIntegerForm(Synchronize), form.Integer(1))),
	}
	for name, f := range tests {
		t.Run(name, func(t *testing.T) {
			buf := new(bytes.Buffer)

			err := NewEncoder(buf).Encode(f, ctx)
			must(t, err)

			t.Logf(hex.Dump(buf.Bytes()))

			g, err := NewDecoder(buf).Decode(ctx)
			must(t, err)

			assert.Equal(t, fmt.Sprint(f), fmt.Sprint(g))
		})
	}
}
