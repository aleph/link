package link

import (
	"errors"
	"fmt"
	"io"
	"log"
	"sync"

	"gitlab.com/aleph-project/form"
	"gitlab.com/go-utils/wait"
)

// A Connection is a connection over an Aleph link for sending and receiving messages
type Connection interface {
	// Init initializes the link
	Init() error
	// Context returns the link context
	Context() Context
	// Execute sends an execution request
	Execute(value form.Form) (form.Form, error)
	// Accept accepts and processes an execution request, and sends the result
	Accept(execute func(form.Form) (form.Form, error)) bool
	// Close terminates the connection
	Close()
}

func debug(msg string) {
	// log.Println(msg)
}

func debugf(fmt string, args ...interface{}) {
	// log.Printf(fmt, args...)
}

type connection struct {
	link   Link
	closer io.Closer
	master bool

	done     bool
	donecond *sync.Cond

	response wait.List
	accept   chan wait.CompletionWithArgument

	closeFin, closeAck wait.Completion
}

// NewConnection creates a connection over an Aleph link
//
// Closer is used to cleanup once the connection is closed.
//
// If closer is not specified, NewConnection may leak goroutines.
func NewConnection(link Link, closer io.Closer, master bool) Connection {
	return &connection{
		link:     link,
		closer:   closer,
		master:   master,
		donecond: sync.NewCond(new(sync.Mutex)),
		response: wait.NewList(),
		accept:   make(chan wait.CompletionWithArgument),
		closeFin: wait.New(nil),
		closeAck: wait.New(nil),
	}
}

func (c *connection) send(kind PacketKind, els ...form.Form) error {
	if c.done {
		return io.EOF
	}

	err := c.link.Send(kind, els...)
	if err == io.EOF {
		debugf("got EOF while sending %v", kind)
		c.stop(false)
	}

	return err
}

func (c *connection) receive() (PacketKind, []form.Form, error) {
	if c.done {
		return InvalidPacket, nil, io.EOF
	}

	kind, els, err := c.link.Receive()
	if err == io.EOF {
		debug("got EOF while receiving")
		c.stop(false)
		return InvalidPacket, nil, err
	}

	return kind, els, err
}

func (c *connection) stop(graceful bool) bool {
	c.donecond.L.Lock()
	if c.done {
		c.donecond.L.Unlock()
		return false
	}

	debugf("stopping (graceful: %v)", graceful)
	c.done = true
	c.donecond.Broadcast()
	c.donecond.L.Unlock()

	c.response.Close()
	close(c.accept)

	if !graceful {
		debug("gracelessly terminating connection")
		return true
	}

	go func() {
		c.closeFin.Wait()
		debug("sending finalization acknowledgement")
		err := c.link.Send(Acknowledge)
		if err != nil {
			log.Printf("error occurred while closing: %v", err)
		}
	}()

	debug("sending finalization request")
	err := c.link.Send(Finalize)
	if err != nil {
		log.Printf("error occurred while closing: %v", err)
		return true
	}
	c.closeAck.Wait()

	return true
}

func (c *connection) Context() Context { return c.link.Context() }

func (c *connection) Init() error {
	if c.master {
		// send Synchronize
		if err := c.send(Synchronize); err != nil {
			return err
		}

		// expect SyncAcknowledge
		ver, err := c.receiveVersion(SyncAcknowledge)
		if err != nil {
			return err
		}

		// set version
		if ver > c.link.Version() {
			ver = c.link.Version()
		}
		c.link.SetActiveVersion(ver)

		// send Acknowledge
		if err := c.send(Acknowledge, form.SmallIntegerForm(c.link.ActiveVersion())); err != nil {
			return err
		}

	} else {
		// expect Synchronize
		if kind, _, err := c.receive(); err != nil {
			return err
		} else if kind != Synchronize {
			return fmt.Errorf("expected Synchronize, got %v", kind)
		}

		// send SyncAcknowledge
		if err := c.send(SyncAcknowledge, form.SmallIntegerForm(c.link.Version())); err != nil {
			return err
		}

		// expect Acknowledge
		ver, err := c.receiveVersion(Acknowledge)
		if err != nil {
			return err
		}

		if ver > c.link.Version() {
			return errors.New("version negotiation failed")
		}

		c.link.SetActiveVersion(ver)
	}

	go c.receiveLoop()
	return nil
}

func (c *connection) receiveVersion(expect PacketKind) (int, error) {
	kind, args, err := c.receive()
	if err != nil {
		return 0, err
	}
	if kind != expect {
		return 0, fmt.Errorf("expected %v, got %v", expect, kind)
	}

	if len(args) == 0 {
		return 0, fmt.Errorf("malformed %v: no arguments", expect)
	}

	ver, ok := args[0].(form.SmallIntegerForm)
	if !ok || ver.Raw() <= 0 || ver.Raw() > int64(MaxInt) {
		return 0, fmt.Errorf("malformed %v: first argument is not a version", expect)
	}

	return int(ver.Raw()), nil
}

func (c *connection) receiveLoop() {
	var closing = false
	var closed = false

	var process = func(kind PacketKind, els []form.Form) {
		switch kind {
		default:
			// ignore
			log.Printf("invalid packet kind: %v\n", kind)

		case Finalize:
			debug("received finalization request")
			closing = true
			c.closeFin.Complete(kind, nil)

		case FinalizeAcknowledge:
			debug("received finalization request and acknowledgement")
			closing = true
			closed = true
			c.closeFin.Complete(kind, nil)
			c.closeAck.Complete(kind, nil)

		case Acknowledge:
			if !closing || closed {
				log.Println("unexpected acknowledge")
				break
			}

			debug("received finalization acknowledgement")
			closed = true
			c.closeAck.Complete(kind, nil)

		case Execute:
			_, ok := els[0].(form.SmallIntegerForm)
			if !ok {
				log.Printf("invalid execution request\n")
				break
			}

			w := wait.NewWithArgument(els[1], nil)
			c.accept <- w
			v, err := w.Wait()

			valuef, ok := v.(form.Form)
			if !ok {
				// silently ignore v is non-nil, non-form
				valuef = form.Null
			}

			errf := form.Form(form.Null)
			if err != nil {
				errf = form.String(err.Error())
			}

			err = c.send(Response, els[0], valuef, errf)
			if err != nil {
				log.Printf("an error occurred while sending a response: %v\n", err)
			}

		case Response:
			if len(els) != 3 {
				log.Printf("invalid execution response\n")
				break
			}

			id, ok := els[0].(form.SmallIntegerForm)
			if !ok {
				log.Printf("invalid execution response\n")
				break
			}

			var err error
			if str, ok := els[2].(form.StringForm); ok {
				// silently ignore els[2] is non-null, non-string
				err = errors.New(str.Raw())
			}

			w, ok := c.response.Find(uint64(id.Raw()))
			if !ok {
				log.Printf("no one waiting for response\n")
				break
			}

			w.Complete(els[1], err)
		}
	}

	debug("receive loop starting")

	for !c.done && !closing {
		kind, els, err := c.receive()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Printf("an error occurred while receiving a packet: %v", err)
			continue
		}

		process(kind, els)
	}

	go c.Close()

	for !closed {
		kind, els, err := c.link.Receive()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Printf("an error occurred while receiving a packet: %v", err)
			continue
		}

		process(kind, els)
	}

	debug("receive loop terminated")
}

func (c *connection) Execute(value form.Form) (form.Form, error) {
	var w = c.response.Next()

	err := c.send(Execute, form.Integer(int64(w.ID())), value)
	if err != nil {
		w.Complete(nil, errors.New("the completion was cancelled due to a send failure"))
		return nil, err
	}

	v, err := w.Wait()
	if v == nil {
		return nil, err
	}
	return v.(form.Form), err
}

func (c *connection) Accept(execute func(form.Form) (form.Form, error)) bool {
	w, ok := <-c.accept
	if !ok {
		return false
	}

	w.Complete(execute(w.Argument().(form.Form)))
	return true
}

func (c *connection) Close() {
	if !c.stop(true) {
		return
	}
}
