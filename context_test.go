package link

import (
	"strconv"
	"sync/atomic"
	"testing"

	"github.com/alecthomas/assert"
)

func BenchmarkRead(b *testing.B) {
	ctx := NewContext()
	for i := byte(0); i < 100; i++ {
		ctx.RegisterHead(string([]byte{i}))
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ctx.GetKnownHeadID("")
	}
}

var x uint64

func BenchmarkReadMap(b *testing.B) {
	ctx := map[string]uint64{}
	for i := byte(0); i < 100; i++ {
		ctx[string([]byte{i})] = uint64(i)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		id, _ := ctx[""]
		x += id
	}
}

func BenchmarkWrite(b *testing.B) {
	ctx := NewContext()
	for i := int64(0); i < int64(b.N); i++ {
		ctx.RegisterHead(strconv.FormatInt(i, 10))
	}
}

func BenchmarkWriteMap(b *testing.B) {
	ctx1 := map[string]uint64{}
	ctx2 := map[uint64]string{}
	for i := int64(0); i < int64(b.N); i++ {
		n := strconv.FormatInt(i, 10)
		ctx1[n] = uint64(i)
		ctx2[uint64(i)] = n
	}
}

func BenchmarkReadWrite(b *testing.B) {
	ctx := NewContext()

	var writer int32

	b.SetParallelism(2) // ensure at least 2 routines

	b.RunParallel(func(pb *testing.PB) {
		// use 1 routine as writer
		if atomic.CompareAndSwapInt32(&writer, 0, 1) {
			var i int64
			for pb.Next() {
				ctx.RegisterHead(strconv.FormatInt(i, 10))
				i++
			}
			return
		}

		for pb.Next() {
			ctx.GetKnownHeadID("")
		}
	})
}

func TestSimple(t *testing.T) {
	ctx := NewContext()
	name := "A"
	id := ctx.RegisterHead(name)

	x, ok := ctx.GetKnownHeadID(name)
	assert.True(t, ok)
	assert.Equal(t, id, x)

	y, ok := ctx.GetKnownHeadName(id)
	assert.True(t, ok)
	assert.Equal(t, name, y)
}
