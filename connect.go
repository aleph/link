package link

import (
	"io"
	"net"
)

// A Listener accepts new connections over Aleph links
type Listener interface {
	// Accept waits for and returns the next connection to the listener
	Accept() (Connection, error)

	// Close closes the listener
	Close() error
}

// Connect creates an Aleph link and a connection over it
func Connect(master bool, rw io.ReadWriteCloser) (Connection, error) {
	c := NewConnection(New(rw, rw), rw, master)
	if err := c.Init(); err != nil {
		return nil, err
	}
	return c, nil
}

// Dial dials a new network connection and creates an Aleph connection with it
func Dial(master bool, network, address string) (Connection, error) {
	conn, err := net.Dial(network, address)
	if err != nil {
		return nil, err
	}
	return Connect(master, conn)
}

// Listen creates a new listener
func Listen(master bool, network, address string) (Listener, error) {
	l, err := net.Listen(network, address)
	if err != nil {
		return nil, err
	}
	return &listener{master, l}, nil
}

type listener struct {
	master   bool
	listener net.Listener
}

func (l *listener) Accept() (Connection, error) {
	conn, err := l.listener.Accept()
	if err != nil {
		return nil, err
	}
	return Connect(l.master, conn)
}

func (l *listener) Close() error {
	return l.listener.Close()
}
