module gitlab.com/aleph-project/link

require (
	github.com/alecthomas/assert v0.0.0-20170929043011-405dbfeb8e38
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	gitlab.com/aleph-project/form v0.2.10
	gitlab.com/go-utils/wait v0.1.0
	golang.org/x/sys v0.0.0-20181210030007-2a47403f2ae5 // indirect
)
