package link

import "math/big"

func convertToZigZag(n interface{}) (interface{}, bool) {
	switch m := n.(type) {
	case int:
		return convertToSmallZigZag(int64(m)), true
	case int8:
		return convertToSmallZigZag(int64(m)), true
	case int16:
		return convertToSmallZigZag(int64(m)), true
	case int32:
		return convertToSmallZigZag(int64(m)), true
	case int64:
		return convertToSmallZigZag(int64(m)), true
	case uint:
		return convertToSmallZigZag(int64(m)), true
	case uint8:
		return convertToSmallZigZag(int64(m)), true
	case uint16:
		return convertToSmallZigZag(int64(m)), true
	case uint32:
		return convertToSmallZigZag(int64(m)), true
	case uint64:
		return convertToSmallZigZag(int64(m)), true

	case big.Int:
		return convertToLargeZigZag(&m), true
	case *big.Int:
		return convertToLargeZigZag(m), true
	}

	return nil, false
}

func convertFromZigZag(n interface{}) (interface{}, bool) {
	switch m := n.(type) {
	case int:
		return convertFromSmallZigZag(uint64(m)), true
	case int8:
		return convertFromSmallZigZag(uint64(m)), true
	case int16:
		return convertFromSmallZigZag(uint64(m)), true
	case int32:
		return convertFromSmallZigZag(uint64(m)), true
	case int64:
		return convertFromSmallZigZag(uint64(m)), true
	case uint:
		return convertFromSmallZigZag(uint64(m)), true
	case uint8:
		return convertFromSmallZigZag(uint64(m)), true
	case uint16:
		return convertFromSmallZigZag(uint64(m)), true
	case uint32:
		return convertFromSmallZigZag(uint64(m)), true
	case uint64:
		return convertFromSmallZigZag(uint64(m)), true

	case big.Int:
		return convertFromLargeZigZag(&m), true
	case *big.Int:
		return convertFromLargeZigZag(m), true
	}

	return nil, false
}

func convertToSmallZigZag(n int64) uint64 {
	if n >= 0 {
		return uint64(2 * n)
	}
	return uint64(2*(^n) + 1)
}

func convertFromSmallZigZag(n uint64) int64 {
	if n&0x1 == 0 {
		// positive
		return int64(n / 2)
	}

	return int64(^(n / 2))
}

func convertToLargeZigZag(n *big.Int) *big.Int {
	if n.Sign() >= 0 {
		return new(big.Int).Lsh(n, 1)
	}

	n.Not(n)
	n.Lsh(n, 1)
	return n.Add(n, big.NewInt(1))
}

func convertFromLargeZigZag(n *big.Int) *big.Int {
	if n.Bit(0) == 0 {
		// positive
		return n.Rsh(n, 1)
	}

	n.Rsh(n, 1)
	return n.Not(n)
}
