package link

import (
	"fmt"
	"testing"

	"github.com/alecthomas/assert"
)

func TestRealHeader(t *testing.T) {
	for _, format := range formats {
		for _, value := range values {
			t.Run(fmt.Sprintf("%v,%v", format, value), func(t *testing.T) {
				f, v := DecodeRealHeader(EncodeRealHeader(format, value))
				assert.Equal(t, format, f)
				assert.Equal(t, value, v)
			})
		}
	}
}

var formats = []RealFormat{
	ArbitraryRealFormat,
	RealBits16Base2Format,
	RealBits32Base2Format,
	RealBits64Base2Format,
	RealBits128Base2Format,
	RealBits256Base2Format,
}

var values = []RealValue{
	EncodedRealValue,
	RealPosZeroValue,
	RealNegZeroValue,
	RealPosInfValue,
	RealNegInfValue,
	RealPosNaNValue,
	RealNegNaNValue,
}
