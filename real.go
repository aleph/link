package link

import "math"

var (
	realPosZeroValue = math.Copysign(0, +1)
	realNegZeroValue = math.Copysign(0, -1)
	realPosInfValue  = math.Inf(+1)
	realNegInfValue  = math.Inf(-1)
	realPosNaNValue  = +math.NaN()
	realNegNaNValue  = -math.NaN()
)

// A RealFormat is a format specifier for encoded real numbers
type RealFormat byte

// A RealValue is a value specifier for encoded real numbers
type RealValue byte

const (
	// ArbitraryRealFormat encodes arbitrary precision real numbers
	ArbitraryRealFormat RealFormat = iota << 4
	// RealBits16Base2Format encodes real numbers as IEEE754 16-bit base-2
	RealBits16Base2Format
	// RealBits32Base2Format encodes real numbers as IEEE754 32-bit base-2
	RealBits32Base2Format
	// RealBits64Base2Format encodes real numbers as IEEE754 64-bit base-2
	RealBits64Base2Format
	// RealBits128Base2Format encodes real numbers as IEEE754 128-bit base-2
	RealBits128Base2Format
	// RealBits256Base2Format encodes real numbers as IEEE754 256-bit base-2
	RealBits256Base2Format
)

const (
	// EncodedRealValue indicates an encoded real number
	EncodedRealValue RealValue = iota
	// RealPosZeroValue represents a positive zero value real number
	RealPosZeroValue
	// RealNegZeroValue represents a negative zero value real number
	RealNegZeroValue
	// RealPosInfValue represents a positive infinity value real number
	RealPosInfValue
	// RealNegInfValue represents a negative infinity value real number
	RealNegInfValue
	// RealPosNaNValue represents a positive not-a-number value real number
	RealPosNaNValue
	// RealNegNaNValue represents a negative not-a-number value real number
	RealNegNaNValue
)

// DecodeRealHeader separates the real header into the format and value specifiers
func DecodeRealHeader(b byte) (RealFormat, RealValue) {
	return RealFormat(b & 0xF0), RealValue(b & 0x0F)
}

// EncodeRealHeader merges the format and value specifiers into the real header
func EncodeRealHeader(f RealFormat, v RealValue) byte {
	return byte(f) | byte(v)
}

// CompressRealValue64 returns the RealValue corresponding to v
func CompressRealValue64(v float64) RealValue {
	switch v {
	case realPosZeroValue:
		return RealPosZeroValue
	case realNegZeroValue:
		return RealNegZeroValue
	case realPosInfValue:
		return RealPosInfValue
	case realNegInfValue:
		return RealNegInfValue
	case realPosNaNValue:
		return RealPosNaNValue
	case realNegNaNValue:
		return RealNegNaNValue
	default:
		return EncodedRealValue
	}
}

// ExpandRealValue64 returns the float64 corresponding to v
func ExpandRealValue64(v RealValue) float64 {
	switch v {
	case RealPosZeroValue:
		return realPosZeroValue
	case RealNegZeroValue:
		return realNegZeroValue
	case RealPosInfValue:
		return realPosInfValue
	case RealNegInfValue:
		return realNegInfValue
	case RealPosNaNValue:
		return realPosNaNValue
	case RealNegNaNValue:
		return realNegNaNValue
	default:
		return math.Pi
	}
}
