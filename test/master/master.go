package main

import (
	"log"
	"math"
	"os"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/link"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var port = "localhost:8080"
	if len(os.Args) > 1 {
		port = os.Args[1]
	}

	l, err := link.Listen(true, "tcp", port)
	if err != nil {
		log.Fatal(err)
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go func() {
			var ok = true
			for ok {
				ok = conn.Accept(func(v form.Form) (form.Form, error) {
					log.Println(v)
					switch u := v.(type) {
					case form.StringForm:
						b := []byte(u)
						n := len(b)
						for i := 0; i < n/2; i++ {
							b[i], b[n-i-1] = b[n-i-1], b[i]
						}
						return form.StringForm(b), nil

					case form.NumericForm:
						v := math.Pow(u.SmallRealValue(), 2)
						return form.Real(v), nil
					}
					return form.Null, nil
				})
			}
		}()
	}
}
