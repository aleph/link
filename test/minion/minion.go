package main

import (
	"log"
	"os"

	"gitlab.com/aleph-project/form"
	"gitlab.com/aleph-project/link"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var args = os.Args[1:]
	var port = "localhost:8080"
	if len(args) > 0 {
		switch args[0] {
		case "-":
		default:
			port = args[0]
		}
		args = args[1:]
	}

	var f form.Form = form.String("Hello World")
	if len(args) > 0 {
		switch args[0] {
		case "-e":
			h := form.NewKnownSymbolicHead(1<<30-1, "Invalid")
			f = form.Must(form.New(h))

		case "-f":
			hf := form.Must(form.New(form.NewUnknownSymbolicHead("Test"), form.Integer(1)))
			h := form.NewFormHead(hf)
			f = form.Must(form.New(h, form.String("hi")))

		default:
			f = form.String(args[0])
		}
		args = args[1:]
	}

	conn, err := link.Dial(false, "tcp", port)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Executing %v", f)
	v, err := conn.Execute(f)
	log.Println(v)
	if err != nil {
		log.Fatal(err)
	}

	conn.Close()
}
