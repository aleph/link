package link

import (
	"sync/atomic"

	"gitlab.com/aleph-project/form"
)

// A Context stores IDs of known heads
type Context interface {
	form.Context
	// RegisterHead registers a new known head and returns the ID
	RegisterHead(name string) uint64
}

// NewContext returns a new context
func NewContext() Context {
	ch := make(chan struct{})
	close(ch)

	return &context{
		notifyOne:  make(chan struct{}, 1),
		notifyMany: ch,

		byID:   make(map[uint64]string),
		byName: make(map[string]uint64),

		nextID: uint64(form.MaxHeadKind),
	}
}

type context struct {
	readers    int32
	notifyOne  chan struct{}
	notifyMany chan struct{}

	byID   map[uint64]string
	byName map[string]uint64

	nextID uint64
}

func (c *context) GetKnownHeadName(id uint64) (name string, ok bool) {
	end := c.readBegin()
	name, ok = c.byID[id]
	end()
	return
}

func (c *context) GetKnownHeadID(name string) (id uint64, ok bool) {
	end := c.readBegin()
	id, ok = c.byName[name]
	end()
	return
}

func (c *context) readBegin() (end func()) {
again:
	// writing sets readers to -1
	var r = c.readers
	if r < 0 {
		// wait on the writer
		<-c.notifyMany
		goto again
	}

	// TODO use atomic.AddInt32
	if !atomic.CompareAndSwapInt32(&c.readers, r, r+1) {
		goto again
	}

	return func() {
	again:
		var r = c.readers
		if r < 1 {
			panic("inconsistent state")
		}
		// TODO use atomic.AddInt32
		if !atomic.CompareAndSwapInt32(&c.readers, r, r-1) {
			goto again
		}

		select {
		case c.notifyOne <- struct{}{}:
		default:
		}
	}
}

func (c *context) writeBegin() (end func()) {
	ch := make(chan struct{})
	end = func() {
		if !atomic.CompareAndSwapInt32(&c.readers, -1, 0) {
			panic("inconsistent state")
		}
		close(ch)

		select {
		case c.notifyOne <- struct{}{}:
		default:
		}
	}

again:
	var r = c.readers
	if r == 0 {
		if !atomic.CompareAndSwapInt32(&c.readers, 0, -1) {
			goto again
		}

		c.notifyMany = ch
		return
	}

	if r < 0 {
		// wait on the other writer
		<-c.notifyMany
		goto again
	}

	// wait for a reader to complete
	<-c.notifyOne
	goto again
}

func (c *context) RegisterHead(name string) uint64 {
	defer c.writeBegin()()

	if id, ok := c.byName[name]; ok {
		return id
	}

	c.nextID++
	var id = c.nextID

	if _, ok := c.byID[id]; ok {
		panic("inconsistent state")
	}

	c.byID[id] = name
	c.byName[name] = id
	return id
}
