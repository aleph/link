package link

import (
	"errors"
	"fmt"
	"io"
	"sync"

	"gitlab.com/aleph-project/form"
)

// PacketKind is the kind of a link packet
type PacketKind int

const (
	// InvalidPacket is not a valid packet kind and the kind of a packet that is
	// not valid
	InvalidPacket PacketKind = iota
	// Acknowledge is the kind of the acknowledgement packet sent during link
	// initialization and closure
	Acknowledge
	// Synchronize is the kind of the synchronization packet sent by the link
	// master
	Synchronize
	// SyncAcknowledge is the kind of the acknowledgement and synchronization
	// packet sent by the link minion
	SyncAcknowledge
	// Finalize is the kind of the finalization packet sent to request link
	// closure
	Finalize
	// FinalizeAcknowledge is the kind of the acknowledgement and finalization
	// packet sent in response to link closure
	FinalizeAcknowledge

	// Execute is the kind of an execution request packet
	Execute
	// Response is the kind of a response packet
	Response
)

func (k PacketKind) String() string {
	switch k {
	case InvalidPacket:
		return "InvalidPacket"
	case Synchronize:
		return "Synchronize"
	case SyncAcknowledge:
		return "SyncAcknowledge"
	case Acknowledge:
		return "Acknowledge"
	case Execute:
		return "Execute"
	case Response:
		return "Response"
	}

	return fmt.Sprintf("PacketKind:%d", k)
}

// A Link is an Aleph link for sending and receiving form packets
type Link interface {
	// Context returns the link context
	Context() Context
	// Version returns the supported link version
	Version() int
	// ActiveVersion returns the active link version
	ActiveVersion() int
	// SetActiveVersion sets the active link version
	SetActiveVersion(int) error
	// Send encodes and sends a packet
	Send(kind PacketKind, els ...form.Form) error
	// Receive receives and decodes a packet
	Receive() (PacketKind, []form.Form, error)
}

// New returns a new Link using the reader and writer for transmission and the
// context for encoding and decoding
func New(r io.Reader, w io.Writer) Link {
	return &link{
		context: NewContext(),
		decoder: NewDecoder(r),
		encoder: NewEncoder(w),
	}
}

type link struct {
	context Context
	decoder StreamDecoder
	encoder StreamEncoder

	slock, rlock sync.Mutex

	activeVersion int
}

func (l *link) Context() Context   { return l.context }
func (l *link) Version() int       { return 1 }
func (l *link) ActiveVersion() int { return l.activeVersion }

func (l *link) SetActiveVersion(ver int) error {
	if ver <= 0 {
		return fmt.Errorf("invalid version: %d", ver)
	}
	if ver > l.Version() {
		return fmt.Errorf("unsupported version: %d", ver)
	}
	l.activeVersion = ver
	return nil
}

func (l *link) Send(kind PacketKind, els ...form.Form) error {
	if kind == InvalidPacket {
		return errors.New("cannot send an invalid packet")
	}

	l.slock.Lock()
	els = append([]form.Form{form.SmallIntegerForm(kind)}, els...)
	f := form.Must(form.New(form.PacketHead, els...))
	err := l.encoder.Encode(f, l.context)
	l.slock.Unlock()
	return err
}

func (l *link) Receive() (PacketKind, []form.Form, error) {
	l.rlock.Lock()
	f, err := l.decoder.Decode(l.context)
	if f == nil {
		l.rlock.Unlock()
		return InvalidPacket, nil, err
	}

	if f.Head().Name() != "Packet" {
		l.rlock.Unlock()
		return InvalidPacket, []form.Form{f}, errors.New("form is not a packet")
	}

	els := f.GetElements()
	if len(els) == 0 {
		l.rlock.Unlock()
		return InvalidPacket, []form.Form{f}, errors.New("packet has no elements")
	}

	kind, ok := els[0].(form.SmallIntegerForm)
	if !ok {
		l.rlock.Unlock()
		return InvalidPacket, []form.Form{f}, errors.New("packet does not have a kind")
	}

	l.rlock.Unlock()
	return PacketKind(kind.Raw()), els[1:], nil
}
